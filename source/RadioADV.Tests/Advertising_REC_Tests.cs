using NUnit.Framework;

namespace Radio_ADV.Tests
{
    [TestFixture, Description("Тестируем класс Advertising_REC")]
    public class Advertising_REC_Tests
    {
        [Test, Description("Тестируем пользовательский конструктор Advertising_REC")]
        public void TestAdvertisingREC_UserConstructor()
        {
            int ID = 002;
            int TIME_REC = 59;
            bool HAS_HASH = true;
            string HASH = "abcdefg";
            // пользовательский конструктор Advertising_REC
            Advertising_REC advertising_REC = new Advertising_REC(ID, TIME_REC, HAS_HASH, HASH);
            Assert.AreEqual(002, advertising_REC.ID);
            Assert.AreEqual(59, advertising_REC.TIME_REC);
            Assert.IsTrue(true, "abcdefg", advertising_REC.HAS_HASH, advertising_REC.HASH);
        }
    }
}