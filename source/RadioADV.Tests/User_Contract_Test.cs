using System;
using System.Collections.Generic;
using NUnit.Framework;
using Radio_ADV.Radio_ADV;

namespace Radio_ADV.Tests
{
    [TestFixture, Description("Тестируем класс Contract")]
    public class User_Contract_Test
    {
        [Test, Description("Тестируем пользовательский конструктор Contract")]
        public void TestContract_UserConstructor()
        {
            int ID = 001;
            int ID_MANAGER = 00001;
            string ADVERTISER = "Yandex";
            var RECORD = new List<Advertising_REC>
            {
                new Advertising_REC(002, 59, true, "abcdef")
            };
            int BROADCAST_FREQUENCY = 25;
            DateTime PLAYBACK_START_DATE = new DateTime(2020, 05, 17);
            DateTime PLAYBACK_FINAL_DATE = new DateTime(2020, 06, 17);
            // пользовательский конструктор Contract
            User_Contract contract_Test = new User_Contract(ID, ID_MANAGER, ADVERTISER, RECORD, BROADCAST_FREQUENCY, PLAYBACK_START_DATE, PLAYBACK_FINAL_DATE);
            Assert.AreEqual(contract_Test.ID, 001); 
            Assert.AreEqual(contract_Test.ID_MANAGER, 00001);
            Assert.AreEqual(contract_Test.ADVERTISER, "Yandex");
            Assert.AreEqual(contract_Test.ADVERTISING, RECORD);
            Assert.AreEqual(contract_Test.BROADCAST_FREQUENCY, 25);
            Assert.AreEqual(contract_Test.PLAYBACK_START_DATE, new DateTime(2020, 05, 17));
            Assert.AreEqual(contract_Test.PLAYBACK_FINAL_DATE, new DateTime(2020, 06, 17));
        }
    }
}