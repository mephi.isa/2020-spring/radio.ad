using System;
using System.Collections.Generic;
using System.Text;

namespace Radio_ADV
{
    public class Advertising_REC
    {
        public int ID { get; }
        public int TIME_REC { get; }
        public string HASH { get; private set; }
        public bool HAS_HASH { get; private set; }
        // пользовательский конструктор Advertising_REC
        public Advertising_REC(int ID, int TIME_REC, bool HAS_HASH, string HASH)
        {
            this.ID = ID;
            this.TIME_REC = TIME_REC;
            this.HASH = HASH;
            this.HAS_HASH = HAS_HASH;
        }
    }
}
