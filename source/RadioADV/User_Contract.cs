using System;
using System.Collections.Generic;

namespace Radio_ADV
{
    namespace Radio_ADV
    {
        public class User_Contract
        {
            public int ID { get; }
            public int ID_MANAGER { get; }
            public string ADVERTISER { get; }
            public List<Advertising_REC> ADVERTISING { get; private set; }
            public int BROADCAST_FREQUENCY { get; }
            public DateTime PLAYBACK_START_DATE { get; }
            public DateTime PLAYBACK_FINAL_DATE { get; }

            // пользовательский конструктор Contract
            public User_Contract(int ID, int ID_MANAGER, string ADVERTISER, List<Advertising_REC> ADVERTISING,
                int BROADCAST_FREQUENCY, DateTime PLAYBACK_START_DATE, DateTime PLAYBACK_FINAL_DATE)
            {
                this.ID = ID;
                this.ID_MANAGER = ID_MANAGER;
                this.ADVERTISER = ADVERTISER;
                this.ADVERTISING = ADVERTISING;
                this.BROADCAST_FREQUENCY = BROADCAST_FREQUENCY;
                this.PLAYBACK_START_DATE = PLAYBACK_START_DATE;
                this.PLAYBACK_FINAL_DATE = PLAYBACK_FINAL_DATE;
            }
        }
    }
}