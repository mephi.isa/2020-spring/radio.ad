﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Radio_ADVER
{
    public class Advertising_REC
    {
        public int ID { get; }
        public int TIME_REC { get; }
        public string HASH { get; private set; }
        public bool HAS_HASH { get; private set; }
        // пользовательский конструктор Advertising_REC
        public Advertising_REC(int ID, int TIME_REC)
        {
            this.ID = ID;
            this.TIME_REC = TIME_REC;
        }
        public void SET_HASH(string HASH)
        {
            this.HASH = HASH;
            HAS_HASH = true;
        }
    }
}
